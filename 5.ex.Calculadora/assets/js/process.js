const {createApp} = Vue;
createApp({
    data(){
        return{
            valorDisplay:"0",
            operador:null,
            numeroAtual:null,
            numeroAnterior:null,
            tamanhoLetra:50 + "px",
        };
    },//Fechamento data

    methods:{
        getNumero(numero){
            this.ajusteTamanhoDisplay();
            if(this.valorDisplay == "0"){
                this.valorDisplay = numero.toString();
            }
            else{
                if(this.operador == "="){
                    this.valorDisplay = "";
                    this.operador = null;
                }
                // this.valorDisplay = this.valorDisplay + numero.toString();
                
                //Adiçao de string simplificada
                this.valorDisplay += numero.toString();
            }
        },//Fechamento get numero

        limpar(){
            this.valorDisplay = "0";
            this.operador = null;
            this.numeroAnterior = null;
            this.numeroAtual = null;
            this.tamanhoLetra = 50 + "px";
        },//Fechamento limpar

        decimal(){
            if(!this.valorDisplay.includes(".")){
                this.valorDisplay += ".";
            }
        },//fechamento decimal

        operacoes(operacao){
           if(this.numeroAtual != null){
                const displayAtual = parseFloat(this.valorDisplay);
                if(this.operador != null){
                    switch(this.operador){
                        case "+": this.valorDisplay = (this.numeroAtual + displayAtual).toFixed([2]).toString();
                        break;
                        case "-": this.valorDisplay = (this.numeroAtual - displayAtual).toString();
                        break;

                        case "/": 
                        if(displayAtual == 0 || this.numeroAtual == 0){
                            this.valorDisplay = "Operação Impossivel";
                        }
                        else{
                            this.valorDisplay = (this.numeroAtual / displayAtual).toString();
                        }
                        break;

                        case "*": this.valorDisplay = (this.numeroAtual * displayAtual).toFixed([2]).toString();
                        break;
                    }//fim do switch
                    
                    this.numeroAnterior = this.numeroAtual;
                    this.numeroAtual = null;

                    if(this.operador != "="){
                        this.operador = null;
                    }

                    //acertar o numero de casas decimais quando o resultado for decimal
                    if(this.valorDisplay.includes(".")){
                        const numDecimal = parseFloat(this.valorDisplay);
                        this.valorDisplay = (numDecimal.toFixed(2)).toString();
                    }
                    
                }//fim do if

                else{
                    this.numeroAnterior = displayAtual;
                }//fim do else
            }//fim do if principal
            
            if (this.valorDisplay === "NaN") {
                this.valorDisplay = "nao e possivel";
                }

            this.operador = operacao;
            this.numeroAtual = parseFloat(this.valorDisplay);
            if(this.operador != "="){
                this.valorDisplay = "0";
            }
            this.ajusteTamanhoDisplay();           
        },//fechamento operacao

        ajusteTamanhoDisplay(){
            if(this.valorDisplay.length >= 31){
                this.tamanhoLetra = 14 + "px";
                return;
            }
            else if(this.valorDisplay.length >= 21){
                this.tamanhoLetra = 20 + "px";
                return;
            }
            else if(this.valorDisplay.length >= 12){
                this.tamanhoLetra = 30 + "px";
                return;
            }
            else{
                this.valorLetra = 50 + "px";
            }
        }

    },//Fechamento methods
}).mount("#app");//Fechamento APP