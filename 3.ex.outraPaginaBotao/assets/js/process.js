const{createApp} = Vue;

createApp({
    data(){
        return{
            paginaInicial:"index.html",
        };
    },

    methods:{
        loadPage: function(){
            window.location.href='pagina2.html';
        },//Fechamento Load Page
    }//Fechamento methods

}).mount("#app");