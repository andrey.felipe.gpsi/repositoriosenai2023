const {createApp} = Vue;
createApp({
    data(){
        return{
            mostrarLista: false,

            //calculadora
            valorDisplay:"0",
            operador:null,
            numeroAtual:null,
            numeroAnterior:null,
            tamanhoLetra:50 + "px",
            //calculadora

            //login
            usuario:'',
            senha:'',
            erro:null,
            sucesso:null,
            //login

            //arrays(Vetores) para armazenamento dos nome de usuarios e senha
            usuarios: ["admin", "andrey", "usuario"],
            senhas: ["demo", "demo", "demo"],
            userAdmin: false,
            mostrarEntrada: false,
            //arrays(Vetores) para armazenamento dos nome de usuarios e senha

            //Variaveis para tratamento das informaçoes dos novos usuarios
            newUsername:"",
            newPassword:"",
            confirmPassword:"",
            //Variaveis para tratamento das informaçoes dos novos usuarios
        };
    },//Fechamento data


    methods:{
        //Login
        login(){
            //simulando uma requisição de login assincrona
            setTimeout(() => {
                if((this.usuario === "admin" && this.senha === "demo") || (this.usuario === "andrey2" && this.senha === "demo") || (this.usuario === "andrey3" && this.senha === "demo") || (this.usuario === "andrey4" && this.senha === "demo")){
                    // alert("Login realizado com sucesso");
                    this.sucesso = "Usuario e Senha Corretos!";
                    
                    setTimeout(function() {
                        window.location.href='calculadora.html';
                    }, 4000);
                    
                }
                else{
                    this.erro = "Usuario ou Senhas Incorretos!";
                    // alert("Usuario ou senha incorretos!");
                    
                }
            }, 400);
        }, //fechamento login

        //Login2.0
        login2(){
            this.mostrarEntrada = false;
            setTimeout(() => {
                this.mostrarEntrada = true;

                //Verificaçao de usuario cadastrado ou não nos arrays
                const index = this.usuarios.indexOf(this.usuario);
                if (index !== -1 && this.senhas[index] === this.senha){
                    this.erro = null;
                    this.sucesso = "Login Efetuado com sucesso!";

                 //registrando o usuario no LocalStorage para lembrete de acesso

                 localStorage.setItem("usuario", this.usuario);
                 localStorage.setItem("senha", this.senha);
                    
                    //verificando se o usuario é admin
                    if(this.usuario === "admin" && index === 0){
                        this.userAdmin = true;
                        this.sucesso = "Você está logado como Admin";
                    } //fechamento do if
                }//fechamento if
                else{
                    this.sucesso = null;
                    this.erro = "Usuario ou senha incorretos!";
                }
                
                }, 1000);
                        
        }, //fechamento login2
        

        getNumero(numero){
            this.ajusteTamanhoDisplay();
            if(this.valorDisplay == "0"){
                this.valorDisplay = numero.toString();
            }
            else{
                if(this.operador == "="){
                    this.valorDisplay = "";
                    this.operador = null;
                }
                // this.valorDisplay = this.valorDisplay + numero.toString();
                
                //Adiçao de string simplificada
                this.valorDisplay += numero.toString();
            }
        },//Fechamento get numero

        limpar(){
            this.valorDisplay = "0";
            this.operador = null;
            this.numeroAnterior = null;
            this.numeroAtual = null;
            this.tamanhoLetra = 50 + "px";
        },//Fechamento limpar

        decimal(){
            if(!this.valorDisplay.includes(".")){
                this.valorDisplay += ".";
            }
        },//fechamento decimal

        operacoes(operacao){
           if(this.numeroAtual != null){
                const displayAtual = parseFloat(this.valorDisplay);
                if(this.operador != null){
                    switch(this.operador){
                        case "+": this.valorDisplay = ((this.numeroAtual + displayAtual).toFixed(2)*1).toString();
                        break;
                        case "-": this.valorDisplay = (this.numeroAtual - displayAtual).toString();
                        break;

                        case "/": 
                        if(displayAtual == 0 || this.numeroAtual == 0){
                            this.valorDisplay = "Operação Impossivel";
                        }
                        else{
                            this.valorDisplay = (this.numeroAtual / displayAtual).toString();
                        }
                        break;

                        case "*": this.valorDisplay = ((this.numeroAtual * displayAtual).toFixed(2)*1).toString();
                        break;
                    }//fim do switch
                    
                    this.numeroAnterior = this.numeroAtual;
                    this.numeroAtual = null;

                    if(this.operador != "="){
                        this.operador = null;
                    }

                    //acertar o numero de casas decimais quando o resultado for decimal
                    if(this.valorDisplay.includes(".")){
                        const numDecimal = parseFloat(this.valorDisplay);
                        this.valorDisplay = (numDecimal.toFixed(2)).toString();
                    }
                    
                }//fim do if

                else{
                    this.numeroAnterior = displayAtual;
                }//fim do else
            }//fim do if principal
            
            if (this.valorDisplay === "NaN") {
                this.valorDisplay = "nao e possivel";
                }

            this.operador = operacao;
            this.numeroAtual = parseFloat(this.valorDisplay);
            if(this.operador != "="){
                this.valorDisplay = "0";
            }
            this.ajusteTamanhoDisplay();           
        },//fechamento operacao

        ajusteTamanhoDisplay(){
            if(this.valorDisplay.length >= 31){
                this.tamanhoLetra = 14 + "px";
                return;
            }
            else if(this.valorDisplay.length >= 21){
                this.tamanhoLetra = 20 + "px";
                return;
            }
            else if(this.valorDisplay.length >= 12){
                this.tamanhoLetra = 30 + "px";
                return;
            }
            else{
                this.valorLetra = 50 + "px";
            }
        },//fechamento ajustetamanhodisplay

        paginaCadastro(){
            this.mostrarEntrada = false;
            // if(this.userAdmin == true){
            const index = this.usuarios.indexOf(this.usuario);
            
                this.erro = null;
                this.sucesso = "Carregando pagina de cadastro";
                this.mostrarEntrada = true;
                //espera estrategica antes do carregamento da pagina
                setTimeout(() => {}, 2000 );

                setTimeout(() => {
                    window.location.href = "cadastro.html";
                }, 1000);
                
        
        },//fechamento paginaCadastro

        adicionarUsuario(){
            this.mostrarEntrada = false;
            this.usuario = localStorage.getItem("usuario");
            this.senha = localStorage.getItem("senha");

            setTimeout(() => {
                this.mostrarEntrada = true;
                if(this.usuario === "admin"){
                    //verificando se o novo usuario é diferente de vazio e se ele ja nao esta cadastrado no sistema
                    if(!this.usuarios.includes(this.newUsername) && this.newUsername !== "" && !this.newUsername.includes(" ")){
                    //validaçao da senha digitada para cadastro no array

                        if(this.newPassword !== "" && !this.newPassword.includes(" ") && this.newPassword === this.confirmPassword){
                            //Inserindo o novo usuario e senha nos arrays
                            this.usuarios.push(this.newUsername);
                            this.senhas.push(this.newPassword);

                            //Atualizando os usuarios recém cadastrados no Local Storage
                            localStorage.setItem("usuarios", JSON.stringify(this.usuarios));
                            localStorage.setItem("senhas", JSON.stringify(this.senhas));

                            this.newUsername="";
                            this.newPassword= "";
                            this.confirmPassword="";

                            this.erro = null;
                            this.sucesso = "Usuario cadastrado com sucesso";
                        }// fechamento if
                        else{
                            this.newUsername="";
                            this.newPassword= "";
                            this.confirmPassword="";

                            this.erro = "Por favor digite uma senha valida";
                            this.sucesso = null;
                        }
                        }//fechamento if includes

                    else{
                        this.newUsername="";
                        this.newPassword= "";
                        this.confirmPassword="";

                        this.sucesso = null;
                        this.erro="Usuario ja cadastrado ou invalido, por favor digite um usuario diferente";
                    }//fechmento else
                }//fechamento if admin

                else{
                    this.newUsername="";
                    this.newPassword= "";
                    this.confirmPassword="";

                    this.erro="Não esta logado como admin";
                }//fechamento else admin
            }, 400);

        },//fechamento adicionar usuario

        listarUsuarios(){
            this.usuario = localStorage.getItem("usuario" || "");
            if(this.usuario === "admin"){
            if(localStorage.getItem("usuarios") && localStorage.getItem("senhas")){
                this.usuarios=JSON.parse(localStorage.getItem("usuarios"));
                this.senhas=JSON.parse(localStorage.getItem("senhas"));
            }//fechamento if
            this.mostrarLista=!this.mostrarLista;
        }
            
        },//fechamento listar usuario
    

        excluirUsuario(usuario){
            this.mostrarEntrada= false;
            if(usuario === "admin"){
                setTimeout(() => {
                    this.mostrarEntrada = true;
                    this.sucesso = null;
                    this.erro="Você não pode excluir um usuario Administrador";
                }, 400);
                return; //Força a saida desse bloco
            } // Fechamento do if

            if(confirm("Tem certeza que deseja excluir usuario?")){
                const index = this.usuarios.indexOf(usuario);
                if(index !== -1){
                    this.usuarios.splice(index, 1);
                    this.senhas.splice(index, 1);

                    //atualiza o array usuario no localStorage
                    localStorage.setItem("usuarios", JSON.stringify(this.usuarios));
                    localStorage.setItem("senhas", JSON.stringify(this.senhas));

                    setTimeout(() => {
                        this.mostrarEntrada = true;
                        this.sucesso="Usuario excluido com sucesso!";
                        this.erro=null;
                    }, 400);
                }//fechamento if index
            } //Fechamento if

        }//fechamento excluir usuario
    },//Fechamento methods
}).mount("#app");//Fechamento APP