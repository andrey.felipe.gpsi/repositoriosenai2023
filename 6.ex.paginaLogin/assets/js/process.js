const {createApp} = Vue;

createApp({
    data(){
        return{
            usuario:'',
            senha:'',
            erro:null,
            sucesso:null,
        } //fechamento return

    }, //fechamento data

    methods:{
        login(){
            //simulando uma requisição de login assincrona
            setTimeout(() => {
                if((this.usuario === "andrey" && this.senha === "demo") || (this.usuario === "demo" && this.senha === "demo")){
                    this.erro = null;
                    // alert("Login realizado com sucesso");
                    this.sucesso = "Usuario e Senha Corretos!";
                }
                else{
                    this.erro = "Usuario ou Senhas Incorretos!";
                    // alert("Usuario ou senha incorretos!");
                    this.sucesso = null
                }
            }, 400);

        }, //fechamento login
    }, //fechamento methods

}).mount("#app");